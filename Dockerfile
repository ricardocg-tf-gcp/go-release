FROM ubuntu:12.04

RUN apt-get update
RUN apt-get install -y golang

ADD server.go /var/server/server.go

EXPOSE 8080

CMD ["go", "run", "/var/server/server.go"]